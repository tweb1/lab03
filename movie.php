<!DOCTYPE html>

<html>
    <head>
        <title>Rancid Tomatoes</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="movie.css" type="text/css" rel="stylesheet" />
    </head>
    <!-- prelevo tramite get il nome film da passare come parametro che sarà il path/directory -->
    <?php
    $directoryPath = $_GET['film'];
    $lines = file($directoryPath . "/info.txt", FILE_IGNORE_NEW_LINES);
    list($movie_title, $yearProd, $ranking) = $lines; // ogni riga in una variabile
    ?>

    <body>
        <div id="top_banner">
            <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes" />
        </div>
        <h1><?= "$movie_title ($yearProd)" ?> </h1>
        <div id="container">
            <div id="details_container">
                <div>
                    <img src="<?= $directoryPath ?>/overview.png" alt="general overview" />
                </div>
                <div id="details_bullet">
                    <?php
                        foreach (file($directoryPath . "/overview.txt") as $overview) {
                            list($head_description, $body_description) = explode(":", trim($overview));
                    ?>
                        <dl>
                            <dt> <?= "$head_description" ?> </dt>
                            <dd> <?= "$body_description" ?> </dd>
                        </dl>
                    <?php } ?>
                </div>
            </div>
            <div id="rotten">
                <div id="rottenbig">
                    <img  src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rottenbig.png" alt="Rotten" />
                    <span id="perc"><?= "$ranking" ?>%</span>
                </div>
                    <?php
                    $reviews = glob($directoryPath . "/review*.txt"); //glob(pattern, flags)
                    $ReviewsIndex = count($reviews);
                    $ReviewsCount = $ReviewsIndex;
                    if ($ReviewsIndex > 10) {
                        $ReviewsCount = 10;
                    }
                    if ($ReviewsCount % 2 == 0) { //fino a quando è divisibile
                        $split = $ReviewsCount / 2;
                    } else {
                        $split = (int) ($ReviewsCount / 2) + 1;
                    }

                    ?>
                    
                    <?php
                    for ($i = 0; $i < $ReviewsCount; $i++) {
       					if ($i == 0) {
       					?>
       			<div id="cont1">
       					<?php 
       					} else if ($i == $split) {
       					?>
       			</div>
       			<div id="cont2">
                    <?php
                    }
                    list($pos1_quote,$pos2_feedback,$pos3_reviewer,$pos4_publisher) = 
                        file($reviews[$i], FILE_IGNORE_NEW_LINES);
                    ?>
                    
                    <p class="text">
                        <img class="img1" src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?= strtolower($pos2_feedback) ?>.gif" alt="<?= strtolower($pos2_feedback) ?>"/>
                        <q><?= "$pos1_quote" ?></q>
                    </p>
                    <p class="reviewer">
                        <img class="img2" src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic" />
                        <?= "$pos3_reviewer" ?> <br />
                        <span class="italic"><?= "$pos4_publisher" ?></span>
                    </p>
                    <?php } ?>
                </div>
                
            </div>

            <p id="footbar">(1-<?= "$ReviewsCount" ?>) of <?= "$ReviewsIndex" ?></p>
        </div>
        
        <div id="validate">
			<a href="http://validator.w3.org/check/referer"><img src="http://webster.cs.washington.edu/w3c-html.png" alt="Validate"></a> 
        <br>
			<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>
        </div>
    </body>
</html>
